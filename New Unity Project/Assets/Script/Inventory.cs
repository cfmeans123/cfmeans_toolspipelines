using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory : MonoBehaviour
{
    public List<Item> weapons = new List<Item>();
    public int itemSlots;

    [SerializeField]
    GameObject bagReference;

    [SerializeField]
    private int startingItems;

    [SerializeField]
    Vector3 position;
}
