using UnityEngine;
using System.IO;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
public static class SaveDataBinaryManager
{
    public static string directory = "/SaveData/";
    public static string filename = "SaveData.txt";

    public static void Save(GameData gd)
    {
        string dir = Application.persistentDataPath + directory;

        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(dir + filename);
        bf.Serialize(file, gd);
        file.Close();

        Debug.Log("Saved GameData in" + dir);
    }

    public static GameData Load()
    {
        string fullPath = Application.persistentDataPath + directory + filename;
        GameData gd = new GameData();

        if (File.Exists(fullPath))
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(fullPath, FileMode.Open);
                gd = (GameData)bf.Deserialize(file);

                return gd;
            }
            catch (SerializationException)
            {
                Debug.Log("File load error");
            }
        }
        else
        {
            Debug.Log("Save does not exist");
        }
        return gd;
    }
}
