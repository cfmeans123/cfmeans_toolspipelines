using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SaveLoadTest : MonoBehaviour
{
    public GameData gd;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveDataBinaryManager.Save(gd);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            gd = SaveDataBinaryManager.Load();
        }
    }
}


[CustomEditor(typeof(SaveLoadTest))]
public class SaveAndLoad : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SaveLoadTest saveUI = (SaveLoadTest)target;
        if (GUILayout.Button("Serialize to Binary Data"))
        {
            SaveDataBinaryManager.Save(saveUI.gd);
        }
        if (GUILayout.Button("Load from Binary Data"))
        {
            saveUI.gd = SaveDataBinaryManager.Load();
        }
        if (GUILayout.Button("Serialize to JSON Data"))
        {
            SaveDataManager.Save(saveUI.gd);
        }
        if (GUILayout.Button("Load from JSON Data"))
        {
            saveUI.gd = SaveDataManager.Load();
        }
    }

}
